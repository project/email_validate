The main goal of this module is to prevent the use of disposal emails (DEA) when registering and updating a user profile.
We have collected a number of actual and effective methods of checking DEA and implemented them in one module.

Features
--------
Module implements next validation:
#### Google Disposable Email
Prevents the usage of disposable emails:
* user+@gmail.com
* user+@googlemail.com
* u.ser@googlemail.com (dots in any place)

if user with email address user@gmail.com already exists.

#### Yandex temporary emails
Prevents the usage of
* u-ser@yandex.com if u.ser@yandex.com already exists
* u.ser@yandex.com if u-ser@yandex.com already exists
* user@(ya.ru|yandex.ru|yandex.by|...) if user with user@yandex.com email already exists and so it is for every domain.
#### E-mail domain black list
Provide internal blacklist which contains the list of email domains to block.
#### Temporary email checking on https://block-temporary-email.com
Implement checking of emails on https://block-temporary-email.com.
(Required to create account and get API key).

All these checks can either be enabled or disabled as needed.

Post-Installation
-----------------
Install the module and enable it as usual, then open configuration page at
Configuration | People | User Email validation (/admin/config/people/email_validate)
Select and save which constraints should be enabled.

Similar projects
----------------
We analyzed similar modules and create a new with wider functinality
and based on modern Drupal constraints API.

 * https://www.drupal.org/project/check_dns
 * https://www.drupal.org/project/dea_blocker
 * https://www.drupal.org/project/stop_spam_regs
 * https://www.drupal.org/project/duplemail
 * https://www.drupal.org/project/unwanted_email_registration

For Developers:
----------------
    // Example of current user email validation.
    $account = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $violations = $account->get('mail')->validate();
