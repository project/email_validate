<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the YandexEmailConstraint constraint.
 */
class YandexEmailConstraintValidator extends ConstraintValidator {

  /**
   * Constraint object.
   *
   * @var \Symfony\Component\Validator\Constraint
   */
  private Constraint $constraint;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $this->constraint = $constraint;
    $email = $value->getString();
    if (!$this->isYandexDomainEmail($email)) {
      return;
    }

    $this->validateDomainDuplication($email);
    $this->validateDotsAndDashes($email);
  }

  /**
   * Add the violation.
   */
  private function violation($message) {
    $this->context->buildViolation($message)
      // @DCG The path depends on entity type. It can be title, name, etc.
      ->atPath('mail')
      ->addViolation();
  }

  /**
   * Check is email contains Yandex domains.
   */
  private function isYandexDomainEmail($email) {
    [, $mail_domain] = explode('@', $email);
    return in_array($mail_domain, $this->constraint::DOMAINS);
  }

  /**
   * Validate temporary emails with dots and dashes symbols.
   */
  private function validateDotsAndDashes($email) {
    [$mail_username, $mail_domain] = explode('@', $email);
    if (strpos($mail_username, '-') !== FALSE) {
      $duplicate_mail = str_replace('-', '.', $mail_username);
    }
    elseif (strpos($mail_username, '.') !== FALSE) {
      $duplicate_mail = str_replace('.', '-', $mail_username);
    }

    if (!isset($duplicate_mail)) {
      return;
    }
    $duplicate_mail .= '@' . $mail_domain;

    // Get users with the same username.
    $ids = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', $duplicate_mail)
      ->execute();

    if (!empty($ids)) {
      $this->violation($this->constraint->dotsAndDashesDuplicationError);
    }

  }

  /**
   * Validate temporary emails contains additional yandex domains.
   */
  private function validateDomainDuplication($email) {

    [$mail_username] = explode('@', $email);

    $duplicate_mails = $this->constraint::DOMAINS;
    array_walk(
      $duplicate_mails, function (&$value, $key) use ($mail_username) {
        $value = $mail_username . '@' . $value;
      }, $mail_username
    );

    // Remove current email.
    $duplicate_mails = array_diff($duplicate_mails, [$email]);

    // Get users with the same username.
    $ids = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', $duplicate_mails, 'IN')
      ->execute();

    if (!empty($ids)) {
      $this->violation($this->constraint->domainDuplicationError);
    }
  }

}
