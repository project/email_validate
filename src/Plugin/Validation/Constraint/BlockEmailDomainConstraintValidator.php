<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the BlockEmailDomainConstraint constraint.
 */
class BlockEmailDomainConstraintValidator extends ConstraintValidator {

  /**
   * Constraint object.
   *
   * @var \Symfony\Component\Validator\Constraint
   */
  private Constraint $constraint;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $this->constraint = $constraint;
    $email = $value->getString();
    $config = \Drupal::config('email_validate.settings')->get($constraint->getKey());

    $block_domains = $this->getBlockedDomainList($config['block_domains']);
    if ($block_domains) {
      [, $mail_domain] = explode('@', $email);
      if (in_array($mail_domain, $block_domains)) {
        $this->violation($this->constraint->error);
      }
    }
  }

  /**
   * Return email domain list from the textarea.
   */
  private function getBlockedDomainList($text) {
    return $text ? explode("\r\n", $text) : [];
  }

  /**
   * Add the violation.
   */
  private function violation($message) {
    $this->context->buildViolation($message)
      // @DCG The path depends on entity type. It can be title, name, etc.
      ->atPath('mail')
      ->addViolation();
  }

}
