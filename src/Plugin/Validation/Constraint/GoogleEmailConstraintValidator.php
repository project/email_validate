<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the GoogleEmailConstraint constraint.
 */
class GoogleEmailConstraintValidator extends ConstraintValidator {

  /**
   * Constraint object.
   *
   * @var \Symfony\Component\Validator\Constraint
   */
  private Constraint $constraint;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $this->constraint = $constraint;
    $email = $value->getString();
    if (!$this->isGoogleDomainEmail($email)) {
      return;
    }

    $this->validateDots($email);
    $this->validatePlus($email);
  }

  /**
   * Check is email contains Google domains.
   */
  private function isGoogleDomainEmail($email) {
    [, $mail_domain] = explode('@', $email);
    return in_array($mail_domain, ['gmail.com', 'googlemail.com']);
  }

  /**
   * Add the violation.
   */
  private function violation($message) {
    $this->context->buildViolation($message)
      // @DCG The path depends on entity type. It can be title, name, etc.
      ->atPath('mail')
      ->addViolation();
  }

  /**
   * Validate temporary emails with dots symbols.
   */
  private function validateDots($email) {
    [$mail_username, $mail_domain] = explode('@', $email);
    if (strpos($mail_username, '.') !== FALSE) {
      $duplicate_mail = str_replace('.', '', $mail_username);
    }

    if (!isset($duplicate_mail)) {
      return;
    }
    $duplicate_mail .= '@' . $mail_domain;

    // Get users with the same username.
    $ids = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', $duplicate_mail)
      ->execute();

    if (!empty($ids)) {
      $this->violation($this->constraint->dotsDuplicationError);
    }

  }

  /**
   * Validate temporary emails with plus symbols.
   */
  private function validatePlus($email) {
    [$mail_username, $mail_domain] = explode('@', $email);
    if (strpos($mail_username, '+') !== FALSE) {
      $parts = explode('+', $mail_username, 2);
      $duplicate_mail = $parts[0];
    }

    if (!isset($duplicate_mail)) {
      return;
    }
    $duplicate_mail .= '@' . $mail_domain;

    // Get users with the same username.
    $ids = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', $duplicate_mail)
      ->execute();

    if (!empty($ids)) {
      $this->violation($this->constraint->plusDuplicationError);
    }

  }

}
