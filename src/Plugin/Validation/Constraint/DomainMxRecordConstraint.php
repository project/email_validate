<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides an Domain MX record constraint.
 *
 * @Constraint(
 *   id = "DomainMxRecordConstraint",
 *   label = @Translation("DomainMxRecordConstraint", context = "Validation"),
 * )
 */
class DomainMxRecordConstraint extends EmailConstraintBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public $title = 'Domain MX record constraint';

  /**
   * {@inheritdoc}
   */
  public $description = 'Check the DNS record of the e-mail domain name';

  /**
   * Constraint error message.
   *
   * @var string
   */
  public $error = 'E-mail domain name is not recognised!';

}
