<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the TempEmailConstraint constraint.
 */
class TemporaryEmailConstraintValidator extends ConstraintValidator {

  /**
   * Constraint object.
   *
   * @var \Symfony\Component\Validator\Constraint
   */
  private Constraint $constraint;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $this->constraint = $constraint;
    $email = $value->getString();

    $config = \Drupal::config('email_validate.settings')->get($constraint->getKey());
    $api_key = $config['api_key'];
    $api_url = $config['api_url'];
    if ($api_key && $api_url) {
      $api_url = str_replace('{email}', $email, $api_url);
      $uri = str_replace('{api_key}', $api_key, $api_url);
      $options = [
        'method' => 'GET',
        'Accept' => 'application/json',
        'headers' => ['x-api-key' => $api_key],
      ];

      try {
        $client = new Client();
        $response = $client->get($uri, $options);
        $data = json_decode($response->getBody()->getContents());
      }
      catch (GuzzleException $e) {
        \Drupal::logger(__CLASS__)->warning($e->getMessage());
        // We return success if remote service doesn't work.
        return TRUE;
      }

      if (isset($data) && $data->temporary === TRUE) {
        $this->violation($this->constraint->error);
      }
    }

  }

  /**
   * Add the violation.
   */
  private function violation($message) {
    $this->context->buildViolation($message)
      // @DCG The path depends on entity type. It can be title, name, etc.
      ->atPath('mail')
      ->addViolation();
  }

}
