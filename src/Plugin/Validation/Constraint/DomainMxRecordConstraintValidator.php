<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the DomainMxRecordConstraintValidator constraint.
 */
class DomainMxRecordConstraintValidator extends ConstraintValidator {

  /**
   * Constraint object.
   *
   * @var \Symfony\Component\Validator\Constraint
   */
  private Constraint $constraint;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $this->constraint = $constraint;
    $email = $value->getString();
    [, $mail_domain] = explode('@', $email);
    if (!checkdnsrr(idn_to_ascii($mail_domain, 0, INTL_IDNA_VARIANT_UTS46), 'MX')) {
      $this->violation($this->constraint->error);
    }
  }

  /**
   * Add the violation.
   */
  private function violation($message) {
    $this->context->buildViolation($message)
      // @DCG The path depends on entity type. It can be title, name, etc.
      ->atPath('mail')
      ->addViolation();
  }

}
