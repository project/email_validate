<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides an TemporaryEmailConstraint constraint.
 *
 * @Constraint(
 *   id = "TemporaryEmailConstraint",
 *   label = @Translation("TemporaryEmailConstraint", context = "Validation"),
 * )
 */
class TemporaryEmailConstraint extends EmailConstraintBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public $title = 'Temporary emails constraints';

  /**
   * {@inheritdoc}
   */
  public $description = 'Check temporary emails on https://block-temporary-email.com';

  /**
   * Constraint error message.
   *
   * @var string
   */
  public $error = 'Disposable e-mail address is not allowed.';

  /**
   * Return Api key setting field.
   */
  public function getSettingsForm($config = []) {
    $default_api_url = 'https://block-temporary-email.com/check/email/{email}';
    return [
      'api_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('API key'),
        '#default_value' => $config['api_key'] ?? '',
      ],
      'api_url' => [
        '#type' => 'textfield',
        '#title' => $this->t('The requested API url'),
        '#default_value' => $config['api_url'] ?? $default_api_url,
        '#description' => $this->t('Available replacement tokens: {email} for email address and {api_key} for api key.<br> Example API url: <b>@api_url</b>',
        ['@api_url' => $default_api_url]),
      ],
    ];
  }

}
