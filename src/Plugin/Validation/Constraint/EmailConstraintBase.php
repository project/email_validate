<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a base class for email validation constraints.
 */
class EmailConstraintBase extends Constraint {

  /**
   * Constraint title.
   *
   * @var string
   */
  public $title;

  /**
   * Constraint description.
   *
   * @var string
   */
  public $description;

  /**
   * Return Constraint additional setting field.
   */
  public function getSettingsForm() {
    return [];
  }

  /**
   * Return Constraint key value.
   */
  public function getKey() {
    return (substr($this::class, strrpos($this::class, '\\') + 1));
  }

}
