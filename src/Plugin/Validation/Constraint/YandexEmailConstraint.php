<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

/**
 * Provides an EmailValidateConstraint constraint.
 *
 * @Constraint(
 *   id = "YandexEmailConstraint",
 *   label = @Translation("YandexEmailConstraint", context = "Validation"),
 * )
 */
class YandexEmailConstraint extends EmailConstraintBase {

  const DOMAINS = [
    'yandex.com',
    'yandex.ru',
    'yandex.by',
    'yandex.kz',
    'ya.ru',
    'narod.ru',
    'yandex.co.il',
    'yandex.com.ge',
    'yandex.az',
    'yandex.lt',
    'yandex.com.tr',
    'yandex.lv',
    'yandex.tj',
    'yandex.fr',
    'yandex.tmv',
    'yandex.ee',
    'yandex.uz',
    'yandex.pl',
    'yandex.com.am',
    'yandex.md',
    'yandex.kg',
    'yandex.fi',
    'yandex.com.ua',
    'yandex.ua',
  ];

  /**
   * {@inheritdoc}
   */
  public $title = 'Yandex emails constraints';

  /**
   * {@inheritdoc}
   */
  public $description = 'Disable the use of Yandex Email Synonyms';

  /**
   * Constraint domain duplication error message.
   *
   * @var string
   */
  public $domainDuplicationError = 'Use synonyms of original email with different domains is not allowed!';

  /**
   * Constraint dots and dashes includes error message.
   *
   * @var string
   */
  public $dotsAndDashesDuplicationError = 'Use synonyms of original email with dots and dashes is not allowed!';

}
