<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

/**
 * Provides an EmailValidateConstraint constraint.
 *
 * @Constraint(
 *   id = "GoogleEmailConstraint",
 *   label = @Translation("GoogleEmailConstraint", context = "Validation"),
 * )
 */
class GoogleEmailConstraint extends EmailConstraintBase {

  /**
   * {@inheritdoc}
   */
  public $title = 'Google emails constraints';

  /**
   * {@inheritdoc}
   */
  public $description = 'Disable the use of Google Email Synonyms';

  /**
   * Constraint dots contains error message.
   *
   * @var string
   */
  public $dotsDuplicationError = 'Use synonyms of original email with dots is not allowed!';

  /**
   * Constraint plus symbol contains error message.
   *
   * @var string
   */
  public $plusDuplicationError = 'Use synonyms of original email with plus symbol is not allowed!';

}
