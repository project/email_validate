<?php

namespace Drupal\email_validate\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides an TemporaryEmailConstraint constraint.
 *
 * @Constraint(
 *   id = "BlockEmailDomainConstraint",
 *   label = @Translation("BlockEmailDomainConstraint", context = "Validation"),
 * )
 */
class BlockEmailDomainConstraint extends EmailConstraintBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public $title = 'Email domains block list';

  /**
   * {@inheritdoc}
   */
  public $description = 'Block emails from specified domains';

  /**
   * Constraint error message.
   *
   * @var string
   */
  public $error = 'Emails with this domain are not permitted!';

  /**
   * Return Blocked mail domains setting field.
   */
  public function getSettingsForm($config = []) {
    return [
      'block_domains' => [
        '#type' => 'textarea',
        '#title' => $this->t('Blocked mail domains'),
        '#default_value' => $config['block_domains'] ?? '',
        '#rows' => 5,
      ],
    ];
  }

}
