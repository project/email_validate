<?php

namespace Drupal\email_validate\Form;

use Drupal\Core\DrupalKernel;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\TypedData\TypedDataTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Email extended validation settings for this site.
 */
class EmailValidateForm extends ConfigFormBase {

  use TypedDataTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The kernel service.
   *
   * @var \Drupal\Core\DrupalKernel
   */
  protected $kernel;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The plugin cache clearer service.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected $pluginCacheClearer;

  /**
   * Constructs an EmailValidateForm object.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The kernel service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $pluginCacheClearer
   *   The plugin cache clearer service.
   */
  public function __construct(FileSystem $file_system, DrupalKernel $kernel, ModuleHandlerInterface $module_handler, CachedDiscoveryClearerInterface $pluginCacheClearer) {
    $this->fileSystem = $file_system;
    $this->kernel = $kernel;
    $this->moduleHandler = $module_handler;
    $this->pluginCacheClearer = $pluginCacheClearer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('kernel'),
      $container->get('module_handler'),
      $container->get('plugin.cache_clearer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_validate_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['email_validate.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configs = $this->config('email_validate.settings')
      ->getRawData();
    $constraints = $this->getConstraintsAll();

    foreach ($constraints as $constraintKey) {
      $constraintClass = 'Drupal\email_validate\Plugin\Validation\Constraint\\' . $constraintKey;
      $constraint = new $constraintClass();
      $constraintSettings = $configs[$constraintKey] ?? [];
      $is_enable = !empty($constraintSettings['enable']);

      $form[$constraintKey] = [
        '#title' => $constraint->title,
        '#type' => 'details',
        '#open' => $is_enable,
        '#tree' => TRUE,
      ];

      $form[$constraintKey]['enable'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable "@name".', ['@name' => $constraint->title]),
        '#default_value' => $is_enable,
        '#description' => $constraint->description,
      ];

      // Add constraint settings fields.
      $form[$constraintKey] += $constraint->getSettingsForm($constraintSettings);
    }

    // Collapsible details.
    $form['#attached']['library'][] = 'core/drupal.collapse';
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $constraints = $this->getConstraintsAll();
    $values = $form_state->getValues();

    foreach ($constraints as $constraint) {
      $this->config('email_validate.settings')
        ->set($constraint, $values[$constraint])
        ->save();
    }

    parent::submitForm($form, $form_state);

    // Reset field base info cache.
    $this->resetCache();
  }

  /**
   * Update field base info for applying the changes.
   *
   * See drupal_flush_all_caches().
   */
  private function resetCache() {
    $kernel = NULL;
    if (!$kernel instanceof DrupalKernel) {
      $this->kernel->invalidateContainer();
      $this->kernel->rebuildContainer();
    }

    // Clear all plugin caches.
    $this->pluginCacheClearer->clearCachedDefinitions();
  }

  /**
   * Return all available Constraints for email field.
   *
   * @return array|int[]|string[]
   *   Array of constraints plugins.
   */
  private function getConstraintsAll() {
    static $cache = [];

    if (!$cache) {
      $module_path = $this->moduleHandler->getModule('email_validate')->getPath();
      $module_driver_path = DRUPAL_ROOT . '/' . $module_path . '/src/Plugin/Validation/Constraint';
      if (is_dir($module_driver_path)) {
        $mask = '/.*Constraint.php$/';
        $constraints = $this->fileSystem->scanDirectory(
          $module_driver_path, $mask, [
            'key' => 'name',
            'recurse' => FALSE,
          ]
        );
      }
      $cache = array_keys($constraints);
    }

    return $cache;
  }

}
