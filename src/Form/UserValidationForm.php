<?php

namespace Drupal\email_validate\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a bulk users email checking.
 */
class UserValidationForm extends FormBase {

  /**
   * User storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * BatchForm constructor.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   */
  public function __construct(UserStorageInterface $user_storage) {
    $this->userStorage = $user_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_validate_users_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Count users number.
    $result = $this->userStorage->getAggregateQuery()
      ->aggregate('uid', 'COUNT')
      ->condition('uid', 1, '>')
      ->accessCheck(FALSE)
      ->execute();
    $count = $result[0]['uid_count'];

    $form['total'] = [
      '#type' => 'markup',
      '#markup' => sprintf('The total number of users: %s', $count),
      '#value' => $count,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run email verification of users'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $users = $this->getUsers();

    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Processing'))
      ->setFinishCallback([$this, 'finishBatch'])
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'))
      ->addOperation([$this, 'processItems'], [$users]);
    batch_set($batch_builder->toArray());
  }

  /**
   * Load all users uid except admin user.
   */
  public function getUsers() {
    return $this->userStorage->getQuery()
      ->condition('uid', 1, '>')
      ->accessCheck(FALSE)
      ->execute();
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, array &$context) {
    // Elements per operation.
    $limit = 50;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($item, $context);

          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now processing user :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($uid, &$context) {
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->userStorage->load($uid);
    // Example of current user email validation.
    /** @var \Symfony\Component\Validator\ConstraintViolationListInterface $violations */
    $violations = $account->get('mail')->validate();
    if ($violations && $violations->count()) {
      $errors = [];
      foreach ($violations->getIterator() as $violation) {
        $errors[] = $violation->getMessage();
      }
      $context['results']['errors'][$account->getEmail()] = implode('', $errors);
    }
  }

  /**
   * Batch finished callback: display batch report.
   *
   * @param bool $success
   *   Indicates whether the batch has completed successfully.
   * @param mixed[] $results
   *   The array of results gathered by the batch processing.
   * @param string[] $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   */
  public function finishBatch($success, $results, $operations) {
    $message = $this->t('Totally checked emails: @count', [
      '@count' => $results['processed'],
    ]);
    $this->messenger()->addStatus($message);

    if (!empty($results['errors'])) {
      $message = $this->t('Number of invalid user  emails: @count', [
        '@count' => count($results['errors']),
      ]);
      $this->messenger()->addError($message);

      foreach ($results['errors'] as $email => $error) {
        $this->messenger()->addWarning($email . ' | ' . $error);
      }
    }
  }

}
